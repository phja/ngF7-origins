import { Logger, F7AppComponent } from '@framework7';
import { Component, OnInit, ElementRef } from '@angular/core';

declare var Dom7: any;

@Component({
    template: ''
})
export class F7PageComponent implements OnInit {
  protected $page;
  constructor(protected elRef: ElementRef) {
    var $$ = Dom7;
    this.$page = $$(this.elRef.nativeElement).find(".page");
  }

  ngOnInit() {
    var $$ = Dom7;
    this.$page = $$(this.elRef.nativeElement).find(".page")
    var pageName = F7PageComponent.toDashSpaced(this.constructor.name);
    this.$page.attr("data-page", pageName);
    this.$page[0].component = this;
    // Logger.log(F7PageComponent.toDashSpaced(this.constructor.name));
    F7AppComponent.f7App.onPageAfterAnimation(pageName, (page)  => {
      this.onPageInit(page);
    });
    let $pageContent = this.$page.find(".page-content");    
    this.$page.find(".page-content").on('scroll', () => {
      this.onPageScroll($pageContent.scrollTop());
    });
  }

  onPageInit(page) {}
  onPageLeave() {}
  onPageScroll(scrollY) {
  }

  public static getPageName(pageComponent):string {
        return F7PageComponent.toDashSpaced(pageComponent.name);
    }

  public static toDashSpaced(componentName) {
    let m = /^([a-zA-Z])(\w*)Component$/g.exec(componentName);
    return m[1].toLowerCase() + m[2].replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
  }
}