import { Component, OnInit, Input, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { Logger } from '@framework7';
@Component({
  selector: 'page-outlet',
  template: '',
})
export class F7PageOutletComponent implements OnInit {
  
  @Input() component;
  constructor(private componentFactoryResolver: ComponentFactoryResolver, private viewRef: ViewContainerRef) { }

  ngOnInit() {
    if (this.component != null) {
      // Logger.log(this.component);
      const factory = this.componentFactoryResolver.resolveComponentFactory(this.component);
      const ref = this.viewRef.createComponent(factory);
      
    }
  }
}