import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { F7AppComponent, F7NavbarComponent, F7PageOutletComponent, F7PageComponent, F7LoadingComponent } from "../index";


@NgModule({
  declarations: [
    F7AppComponent,
    F7NavbarComponent,
    F7PageOutletComponent,
    F7PageComponent,
    F7LoadingComponent
  ],
  entryComponents: [F7LoadingComponent],
  exports: [
    F7AppComponent,
    F7NavbarComponent,
    F7PageOutletComponent,
    F7PageComponent,
    F7LoadingComponent
  ],
  imports: [
    BrowserModule
  ]
})
export class F7AppModule { }