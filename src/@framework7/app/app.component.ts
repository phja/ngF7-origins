import { Logger } from './../logger';
import { HomeComponent } from './../../app/home/home.component';
import { AboutComponent } from './../../app/about/about.component';
import { TimelinesPageComponent } from './../../app/timeline/timelines-page.component';
import { Component, OnInit, ComponentFactoryResolver, ComponentRef, ViewContainerRef, ViewChild, NgModule } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { F7PageComponent } from "../page/page.component";
import { F7PageOutletComponent } from "../page/page-outlet.component";
import { debug } from 'util';

declare var Framework7: any;
declare var Dom7: any;
declare var routine: any;

declare var cordova: any;
declare var StatusBar: any;
document.addEventListener('deviceready', function () {
  // cordova.plugins.backgroundMode.enable();
  // StatusBar.overlaysWebView(false);
}, false);

@Component({
  selector: 'loading-component',
  template: ''
})
export class F7LoadingComponent { }

@Component({
  // selector: 'app-root',
  template: '',
})
export class F7AppComponent implements OnInit {
  @ViewChild(F7PageOutletComponent, {read: ViewContainerRef}) pageOutlet;
  title = 'ngFramework7';
  static f7App = new Framework7({
    material: true,
    swipePanel: 'right',
    swipePanelActiveArea: 50,
    swipePanelNoFollow: true
    // swipeBackPage: false,
    // swipePanelCloseOpposite:false,
    // swipePanelOnlyClose:false,
  });
  $$ = Dom7;
  mainView: any = null;

  pages = new Set();
  routerConfig: any;
  routeEngine = routine();

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private router:Router) {
    this.routerConfig = Object.assign({}, this.router.config);

    this.router.resetConfig([{ path: '**', component: F7LoadingComponent }]);

    for (let routeIndex in this.routerConfig)
    {
      let route = this.routerConfig[routeIndex];    
      this.routeEngine.on(route.path, ()=> {
        if(route.redirectTo)
          this.routeEngine.goTo(route.redirectTo);
        else {
          this.pages.add(route.component);
          setTimeout(() => {
            let pageName = F7PageComponent.getPageName(route.component);

            console.log(Dom7('.page[data-page="' + pageName + '"]')[0]);
            // Dom7('.page[data-page="' + pageName + '"]')[0].f7PageInitialized = false;

            this.mainView.router.load({pageName: pageName})
          }, 0);
        }
      });
    }

  }

  ngOnInit() {
    this.mainView = F7AppComponent.f7App.addView('.view-main', {domCache: true});
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        if(this.mainView != null)
        {
          let regex = /[\/\\]*(.*)/g;
          let url = event.url;
          url = regex.exec(url)[1];
          console.log(F7AppComponent.f7App.getCurrentView());
          debugger;
          let currentActivePage = F7AppComponent.f7App.getCurrentView().activePage;
          if (currentActivePage)
            currentActivePage.container.component.onPageLeave();
          this.routeEngine.goTo(url);
          setTimeout(() => {
            if(currentActivePage)
              Dom7(currentActivePage.container).addClass("cached");
          }, 500);
        }
      }
      // NavigationStart
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
  }

  public static get F7App() {
      return F7AppComponent.f7App;
  }
}