export class Logger {
    public static log(message, color= '#00ABEC', background = 'white') {
        console.log("%c" + message, "font-style: italic; background: " + background + "; color: " + color);
    }

    public static dir(message, obj = null, color= '#E5446D', background = 'white') {
        if(obj == null)
            obj = message;
        else
            Logger.log(message, color, background);

        console.dir(obj, "background: " + background + "; color: " + color);
    }
    public static success(message, color = '#A0C701', background = 'white') {
        console.log("%c" + message, "background: " + background + "; color: " + color);
    }
}