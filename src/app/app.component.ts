import { Component, OnInit, ComponentFactoryResolver, ComponentRef, ViewContainerRef, ViewChild, NgModule } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { F7AppComponent } from '@framework7';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent extends F7AppComponent {
  title = 'ngFramework7';
}
