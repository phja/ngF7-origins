import { AppComponent } from './../app.component';
// import { F7PageComponent } from '../../components/page/page.component';
import { F7AppComponent, F7PageComponent } from '@framework7';
import { Component, OnInit, ElementRef, HostListener, ViewEncapsulation } from '@angular/core';
import { Logger } from '@framework7';

declare var Dom7: any;
declare var statusbarTransparent: any;
declare var Swiper: any;
declare var plugins: any;
declare var videojs: any;
declare var $: any;
declare var cordova: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent extends F7PageComponent implements OnInit {
  $newsSlider;
  newsSliderAspectRatio = 1/2;
  height;
  myPlayer;
  $pullToRefresh;

  ngOnInit() {
    super.ngOnInit();

    this.$newsSlider = this.$page.find("#news-slider");

    new Swiper(this.$newsSlider[0], {
      loop: true,
      spaceBetween: 0,
      observer: true,
      pagination: ".swiper-pagination-c1"
    });

    var cardTitle = $(this.$page.find(".card-title")[0]);
    this.myPlayer = videojs('example_video_1');

    var $controlBar = $("#example_video_1 .vjs-control-bar");
    // debugger;
    // $controlBar.attrchange(
    //   {
    //     callback: (event) => {
    //       Logger.log("Changed");
    //       if ($controlBar.hasClass("vjs-user-inactive")) {
    //         Logger.log("Inactive");
    //         cardTitle.animate({ "padding-bottom": "0px" }, 'fast'); //.fadeOut();
    //       }
    //       else {
    //         Logger.log("Active");
    //         cardTitle.animate({ "padding-bottom": "30px" }, 'fast'); //.fadeOut();
    //       }
    //     }
    //   });

    // $(".card-header").on("tap", (event) => {
    //   Logger.log("TAP");
    //   this.myPlayer.pause();
    // });

    this.myPlayer.on('pause', () => {
      // Logger.log("Paused");
      // cardTitle.fadeIn();
    });
    this.myPlayer.on('play',() => {
      // Logger.log("Play");
        cardTitle.animate({ "padding-bottom": "30px" }, 'fast').fadeOut();
        this.myPlayer.play();
    });
    this.myPlayer.on('ended',() => {
      // Logger.log("Play");
        cardTitle.animate({ "padding-bottom": "30px" }, 'fast').fadeIn();
    });
    this.myPlayer.on('seeked', function () {
      // Logger.log("Seeked");
    });
    this.myPlayer.on('useractive', ()=>{
      if (this.myPlayer.paused() == false)
        cardTitle.animate({ "padding-bottom": "30px" }, 'fast'); //.fadeOut();
    });

    this.myPlayer.on('userinactive', () => {
      if (this.myPlayer.paused() == false)
        cardTitle.animate({ "padding-bottom": "0px" }, 'fast'); //.fadeOut();
    });

    setTimeout(() => {
      $("#news-slider").animate({opacity: 1}, 'slow', () => {
        setTimeout(() => {
          $("#first-card").animate({opacity: 1}, 'slow', () => {
            $("#home-rest").animate({opacity: 1}, 'slow');
          })
        }, 500);
      })
    }, 500);

    this.$pullToRefresh = Dom7(".pull-to-refresh-content");
    this.$pullToRefresh.on('ptr:pullstart', () => {
      this.$pullToRefresh.find(".pull-to-refresh-layer").animate({opacity: 1}, 'fast');
    });

    this.$pullToRefresh.on('ptr:refresh', () => {
      setTimeout(() => {
          AppComponent.F7App.pullToRefreshDone();
          this.$pullToRefresh.find(".pull-to-refresh-layer").animate({opacity: 0}, 'fast');
      }
      , 4000);
    });

    Dom7(".panel-right").css("opacity", 1);
    this.onResize();
    // Logger.log("Child ngOnInit");
  }

  onPageInit(page) {
  }

  onPageScroll(scrollY) {
    if(scrollY > 0)
      F7AppComponent.f7App.destroyPullToRefresh(this.$pullToRefresh);
    else
      F7AppComponent.f7App.initPullToRefresh(this.$pullToRefresh);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event = null) {
    var $$ =  Dom7;
    this.height = this.$newsSlider.width() * this.newsSliderAspectRatio;
  }
}
