import { Component, OnInit } from '@angular/core';
import { F7PageComponent } from '@framework7';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent extends F7PageComponent {

}
