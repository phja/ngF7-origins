import { Logger } from './../../@framework7/logger';
import { AppComponent } from './../app.component';
import { F7PageComponent } from '@framework7';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

declare var Framework7: any;
declare var statusbarTransparent: any;
declare var Dom7: any;
declare var cordova: any;

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewsPageComponent extends F7PageComponent implements OnInit {

  ngOnInit() {
    super.ngOnInit();

    var $$ = Dom7;
    
    // $$('.show-tab-1').on('click', function () {
    //     AppComponent.F7App.showTab('#tab-1');
    // });
    // $$('.show-tab-2').on('click', function () {
    //     AppComponent.F7App.showTab('#tab-2');
    // });
    // $$('.show-tab-3').on('click', function () {
    //     AppComponent.F7App.showTab('#tab-3');
    // });    
  }

  onPageInit() {
    AppComponent.F7App.showTab('#tab-1');
  }
}
