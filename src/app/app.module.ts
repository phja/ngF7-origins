import { TimelinesPageComponent } from './timeline/timelines-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {F7AppModule} from '@framework7';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';

const appRoutes: Routes =
[
  { path: 'about', component: AboutComponent },
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'news', component: NewsPageComponent},
  { path: 'timelines', component: TimelinesPageComponent},
  { path: 'contact', component: ContactPageComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    TimelinesPageComponent,
    NewsPageComponent,
    ContactPageComponent
  ],
  imports: [
    BrowserModule,
    F7AppModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, useHash: true  }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
