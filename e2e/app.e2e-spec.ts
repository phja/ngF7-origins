import { NgF7Page } from './app.po';

describe('ng-f7 App', () => {
  let page: NgF7Page;

  beforeEach(() => {
    page = new NgF7Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
