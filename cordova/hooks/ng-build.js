const fs = require('fs');
const execSync = require('child_process').execSync;

module.exports = function(context) {
    console.log('\n~ Building Angular application\n');
    const basePath = context.opts.projectRoot;
    const baseWWW = basePath + '/www';

    console.log(execSync(
      "ng build --bh . --output-path cordova/www/",
      {
        maxBuffer: 1024*1024,
        cwd: basePath + '/..'
      }).toString('utf8')
    );
    console.log (". Finished ng build\n")
};