webpackJsonp([2],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/@framework7/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__page_page_component__ = __webpack_require__("../../../../../src/@framework7/page/page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__page_page_outlet_component__ = __webpack_require__("../../../../../src/@framework7/page/page-outlet.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return F7LoadingComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return F7AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




document.addEventListener('deviceready', function () {
    // cordova.plugins.backgroundMode.enable();
    // StatusBar.overlaysWebView(false);
}, false);
var F7LoadingComponent = (function () {
    function F7LoadingComponent() {
    }
    return F7LoadingComponent;
}());
F7LoadingComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'loading-component',
        template: ''
    })
], F7LoadingComponent);

var F7AppComponent = F7AppComponent_1 = (function () {
    function F7AppComponent(componentFactoryResolver, router) {
        var _this = this;
        this.componentFactoryResolver = componentFactoryResolver;
        this.router = router;
        this.title = 'ngFramework7';
        this.$$ = Dom7;
        this.mainView = null;
        this.pages = new Set();
        this.routeEngine = routine();
        this.routerConfig = Object.assign({}, this.router.config);
        this.router.resetConfig([{ path: '**', component: F7LoadingComponent }]);
        var _loop_1 = function (routeIndex) {
            var route = this_1.routerConfig[routeIndex];
            this_1.routeEngine.on(route.path, function () {
                if (route.redirectTo)
                    _this.routeEngine.goTo(route.redirectTo);
                else {
                    _this.pages.add(route.component);
                    setTimeout(function () {
                        var pageName = __WEBPACK_IMPORTED_MODULE_2__page_page_component__["a" /* F7PageComponent */].getPageName(route.component);
                        console.log(Dom7('.page[data-page="' + pageName + '"]')[0]);
                        // Dom7('.page[data-page="' + pageName + '"]')[0].f7PageInitialized = false;
                        _this.mainView.router.load({ pageName: pageName });
                    }, 0);
                }
            });
        };
        var this_1 = this;
        for (var routeIndex in this.routerConfig) {
            _loop_1(routeIndex);
        }
    }
    F7AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mainView = F7AppComponent_1.f7App.addView('.view-main', { domCache: true });
        this.router.events.subscribe(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]) {
                if (_this.mainView != null) {
                    var regex = /[\/\\]*(.*)/g;
                    var url = event.url;
                    url = regex.exec(url)[1];
                    var currentActivePage_1 = F7AppComponent_1.f7App.getCurrentView().activePage;
                    if (currentActivePage_1)
                        currentActivePage_1.container.component.onPageLeave();
                    _this.routeEngine.goTo(url);
                    setTimeout(function () {
                        if (currentActivePage_1)
                            Dom7(currentActivePage_1.container).addClass("cached");
                    }, 500);
                }
            }
            // NavigationStart
            // NavigationEnd
            // NavigationCancel
            // NavigationError
            // RoutesRecognized
        });
    };
    Object.defineProperty(F7AppComponent, "F7App", {
        get: function () {
            return F7AppComponent_1.f7App;
        },
        enumerable: true,
        configurable: true
    });
    return F7AppComponent;
}());
F7AppComponent.f7App = new Framework7({
    material: true,
    swipePanel: 'right',
    swipePanelActiveArea: 50,
    swipePanelNoFollow: true
    // swipeBackPage: false,
    // swipePanelCloseOpposite:false,
    // swipePanelOnlyClose:false,
});
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3__page_page_outlet_component__["a" /* F7PageOutletComponent */], { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* ViewContainerRef */] }),
    __metadata("design:type", Object)
], F7AppComponent.prototype, "pageOutlet", void 0);
F7AppComponent = F7AppComponent_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        // selector: 'app-root',
        template: '',
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ComponentFactoryResolver */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ComponentFactoryResolver */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
], F7AppComponent);

var F7AppComponent_1, _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/@framework7/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return F7AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var F7AppModule = (function () {
    function F7AppModule() {
    }
    return F7AppModule;
}());
F7AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__index__["b" /* F7AppComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["c" /* F7NavbarComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["d" /* F7PageOutletComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["e" /* F7PageComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["f" /* F7LoadingComponent */]
        ],
        entryComponents: [__WEBPACK_IMPORTED_MODULE_2__index__["f" /* F7LoadingComponent */]],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__index__["b" /* F7AppComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["c" /* F7NavbarComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["d" /* F7PageOutletComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["e" /* F7PageComponent */],
            __WEBPACK_IMPORTED_MODULE_2__index__["f" /* F7LoadingComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
        ]
    })
], F7AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/@framework7/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__navbar_navbar_component__ = __webpack_require__("../../../../../src/@framework7/navbar/navbar.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__navbar_navbar_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__page_page_outlet_component__ = __webpack_require__("../../../../../src/@framework7/page/page-outlet.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__page_page_outlet_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__page_page_component__ = __webpack_require__("../../../../../src/@framework7/page/page.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_2__page_page_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_component__ = __webpack_require__("../../../../../src/@framework7/app/app.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__app_app_component__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_3__app_app_component__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__("../../../../../src/@framework7/app/app.module.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_4__app_app_module__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__logger__ = __webpack_require__("../../../../../src/@framework7/logger.ts");
/* unused harmony namespace reexport */






//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/@framework7/logger.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Logger */
var Logger = (function () {
    function Logger() {
    }
    Logger.log = function (message, color, background) {
        if (color === void 0) { color = '#00ABEC'; }
        if (background === void 0) { background = 'white'; }
        console.log("%c" + message, "font-style: italic; background: " + background + "; color: " + color);
    };
    Logger.dir = function (message, obj, color, background) {
        if (obj === void 0) { obj = null; }
        if (color === void 0) { color = '#E5446D'; }
        if (background === void 0) { background = 'white'; }
        if (obj == null)
            obj = message;
        else
            Logger.log(message, color, background);
        console.dir(obj, "background: " + background + "; color: " + color);
    };
    Logger.success = function (message, color, background) {
        if (color === void 0) { color = '#A0C701'; }
        if (background === void 0) { background = 'white'; }
        console.log("%c" + message, "background: " + background + "; color: " + color);
    };
    return Logger;
}());

//# sourceMappingURL=logger.js.map

/***/ }),

/***/ "../../../../../src/@framework7/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar-inner\">\n  <!-- We have home navbar without left link-->\n  <div class=\"center sliding\">{{title}}</div>\n  <div class=\"right\">\n    <!-- Right link contains only icon - additional \"icon-only\" class--><a href=\"#\" class=\"link icon-only open-panel\"> <i class=\"icon icon-bars\"></i></a>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/@framework7/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return F7NavbarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var F7NavbarComponent = (function () {
    function F7NavbarComponent() {
    }
    return F7NavbarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Input */])(),
    __metadata("design:type", String)
], F7NavbarComponent.prototype, "title", void 0);
F7NavbarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'navbar',
        template: __webpack_require__("../../../../../src/@framework7/navbar/navbar.component.html"),
        host: { 'class': 'navbar' }
    })
], F7NavbarComponent);

//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "../../../../../src/@framework7/page/page-outlet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return F7PageOutletComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var F7PageOutletComponent = (function () {
    function F7PageOutletComponent(componentFactoryResolver, viewRef) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.viewRef = viewRef;
    }
    F7PageOutletComponent.prototype.ngOnInit = function () {
        if (this.component != null) {
            // Logger.log(this.component);
            var factory = this.componentFactoryResolver.resolveComponentFactory(this.component);
            var ref = this.viewRef.createComponent(factory);
        }
    };
    return F7PageOutletComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Input */])(),
    __metadata("design:type", Object)
], F7PageOutletComponent.prototype, "component", void 0);
F7PageOutletComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'page-outlet',
        template: '',
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ComponentFactoryResolver */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ComponentFactoryResolver */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* ViewContainerRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* ViewContainerRef */]) === "function" && _b || Object])
], F7PageOutletComponent);

var _a, _b;
//# sourceMappingURL=page-outlet.component.js.map

/***/ }),

/***/ "../../../../../src/@framework7/page/page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return F7PageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var F7PageComponent = F7PageComponent_1 = (function () {
    function F7PageComponent(elRef) {
        this.elRef = elRef;
        var $$ = Dom7;
        this.$page = $$(this.elRef.nativeElement).find(".page");
    }
    F7PageComponent.prototype.ngOnInit = function () {
        var _this = this;
        var $$ = Dom7;
        this.$page = $$(this.elRef.nativeElement).find(".page");
        var pageName = F7PageComponent_1.toDashSpaced(this.constructor.name);
        this.$page.attr("data-page", pageName);
        this.$page[0].component = this;
        // Logger.log(F7PageComponent.toDashSpaced(this.constructor.name));
        __WEBPACK_IMPORTED_MODULE_0__framework7__["b" /* F7AppComponent */].f7App.onPageAfterAnimation(pageName, function (page) {
            _this.onPageInit(page);
        });
        var $pageContent = this.$page.find(".page-content");
        this.$page.find(".page-content").on('scroll', function () {
            _this.onPageScroll($pageContent.scrollTop());
        });
    };
    F7PageComponent.prototype.onPageInit = function (page) { };
    F7PageComponent.prototype.onPageLeave = function () { };
    F7PageComponent.prototype.onPageScroll = function (scrollY) {
    };
    F7PageComponent.getPageName = function (pageComponent) {
        return F7PageComponent_1.toDashSpaced(pageComponent.name);
    };
    F7PageComponent.toDashSpaced = function (componentName) {
        var m = /^([a-zA-Z])(\w*)Component$/g.exec(componentName);
        return m[1].toLowerCase() + m[2].replace(/([A-Z])/g, function (g) { return "-" + g[0].toLowerCase(); });
    };
    return F7PageComponent;
}());
F7PageComponent = F7PageComponent_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* Component */])({
        template: ''
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["l" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_core__["l" /* ElementRef */]) === "function" && _a || Object])
], F7PageComponent);

var F7PageComponent_1, _a;
//# sourceMappingURL=page.component.js.map

/***/ }),

/***/ "../../../../../src/app/about/about.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "  <!-- Page, data-page contains page name-->\n  <div class=\"page\">\n    <!-- Scrollable page content-->\n    <div class=\"page-content\">\n      <div class=\"content-block\">\n        <div class=\"content-block-inner\">\n          <p>Here is About page!</p>\n\n          <p>Go <a routerLink=\"/home\" class=\"external back\" routerLinkActive=\"active\">back</a> or click  <a href=\"#\" class=\"create-page\">here </a>to create dynamic page.</p>\n          <p>Mauris posuere sit amet metus id venenatis. Ut ante dolor, tempor nec commodo rutrum, varius at sem. Nullam ac nisi non neque ornare pretium. Nulla mauris mauris, consequat et elementum sit amet, egestas sed orci. In hac habitasse platea dictumst.</p>\n          <p>Fusce eros lectus, accumsan eget mi vel, iaculis tincidunt felis. Nulla tincidunt pharetra sagittis. Fusce in felis eros. Nulla sit amet aliquam lorem, et gravida ipsum. Mauris consectetur nisl non sollicitudin tristique. Praesent vitae metus ac quam rhoncus mattis vel et nisi. Aenean aliquet, felis quis dignissim iaculis, lectus quam tincidunt ligula, et venenatis turpis risus sed lorem. Morbi eu metus elit. Ut vel diam dolor.</p>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AboutComponent = (function (_super) {
    __extends(AboutComponent, _super);
    function AboutComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AboutComponent;
}(__WEBPACK_IMPORTED_MODULE_1__framework7__["e" /* F7PageComponent */]));
AboutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'app-about',
        template: __webpack_require__("../../../../../src/app/about/about.component.html"),
        styles: [__webpack_require__("../../../../../src/app/about/about.component.css")]
    })
], AboutComponent);

//# sourceMappingURL=about.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Views-->\n<div class=\"views status-bar-overlays\">\n  <!-- Your main view, should have \"view-main\" class-->\n  <div class=\"view view-main\">\n    <!-- Top Navbar-->\n    <!-- <navbar [title]='title'></navbar> -->\n    <!-- Pages, because we need fixed-through navbar and toolbar, it has additional appropriate classes-->\n    <div class=\"pages navbar-through toolbar-through\">\n      <page-outlet *ngFor=\"let page of pages\" [component]=\"page\"></page-outlet>\n      <router-outlet></router-outlet>\n    </div>\n    <!-- Bottom Toolbar-->\n    <div class=\"toolbar\">\n      <div class=\"toolbar-inner\">\n        <a href=\"#\" class=\"link open-login-screen\">\n          <i class=\"material-icons\">fingerprint</i>&nbsp;تسجيل دخول\n        </a>\n        \n        <a routerLink=\"contact\" class=\"link\">\n          تواصل\n          &nbsp;\n          <i class=\"material-icons\">contacts</i>\n        </a>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AppComponent = (function (_super) {
    __extends(AppComponent, _super);
    function AppComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.title = 'ngFramework7';
        return _this;
    }
    return AppComponent;
}(__WEBPACK_IMPORTED_MODULE_1__framework7__["b" /* F7AppComponent */]));
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html")
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__timeline_timelines_page_component__ = __webpack_require__("../../../../../src/app/timeline/timelines-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__about_about_component__ = __webpack_require__("../../../../../src/app/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__news_page_news_page_component__ = __webpack_require__("../../../../../src/app/news-page/news-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__contact_page_contact_page_component__ = __webpack_require__("../../../../../src/app/contact-page/contact-page.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_5__about_about_component__["a" /* AboutComponent */] },
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_7__home_home_component__["a" /* HomeComponent */] },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'news', component: __WEBPACK_IMPORTED_MODULE_8__news_page_news_page_component__["a" /* NewsPageComponent */] },
    { path: 'timelines', component: __WEBPACK_IMPORTED_MODULE_0__timeline_timelines_page_component__["a" /* TimelinesPageComponent */] },
    { path: 'contact', component: __WEBPACK_IMPORTED_MODULE_9__contact_page_contact_page_component__["a" /* ContactPageComponent */] },
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__about_about_component__["a" /* AboutComponent */],
            __WEBPACK_IMPORTED_MODULE_7__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_0__timeline_timelines_page_component__["a" /* TimelinesPageComponent */],
            __WEBPACK_IMPORTED_MODULE_8__news_page_news_page_component__["a" /* NewsPageComponent */],
            __WEBPACK_IMPORTED_MODULE_9__contact_page_contact_page_component__["a" /* ContactPageComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__framework7__["a" /* F7AppModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* RouterModule */].forRoot(appRoutes, { enableTracing: false, useHash: true })
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/contact-page/contact-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".page-content {\n    padding-bottom: 48px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact-page/contact-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page\">\n  <div class=\"navbar\">\n    <div class=\"navbar-inner\">\n      <div class=\"left\"><a href=\"#\" class=\"link open-panel icon-only\" data-panel=\"right\"><i class=\"icon icon-bars\"></i></a></div>\n      <div class=\"center\">تواصل معنا</div>\n      <div class=\"right\"><a routerLink=\"home\" class=\"back link icon-only\"><i class=\"material-icons\">arrow_back</i></a></div>\n    </div>\n  </div>\n  <div class=\"page-content\">\n    <div class=\"content-block-title bold-font\">معلومات أساسية</div>\n    <form class=\"list-block inputs-list\">\n      <ul>\n        <li>\n\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">person_outline</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title floating-label\">الاسم</div>\n              <div class=\"item-input\">\n                <input type=\"text\" placeholder=\"الاسم كامل\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">email</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title floating-label\">البريد الالكتروني</div>\n              <div class=\"item-input\">\n                <input type=\"email\" placeholder=\"البريد الالكتروني\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">language</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title floating-label\">الموقع الكتروني</div>\n              <div class=\"item-input\">\n                <input type=\"url\" placeholder=\"الموقع الالكتروني\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">lock_outline</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title floating-label\">كلمة المرور</div>\n              <div class=\"item-input\">\n                <input type=\"password\" placeholder=\"كلمة المرور\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">call</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title floating-label\">هاتف</div>\n              <div class=\"item-input\">\n                <input type=\"tel\" placeholder=\"الهاتف\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">people_outline</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title label\">النوع</div>\n              <div class=\"item-input\">\n                <select>\n                  <option>ذكر</option>\n                  <option>أنثي</option>\n                </select>\n              </div>\n            </div>\n          </div>\n        </li>\n        <li>\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">today</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title label\">تاريخ الميلاد</div>\n              <div class=\"item-input\">\n                <input type=\"date\" placeholder=\"تاريخ الميلاد\" value=\"2014-04-30\" />\n              </div>\n            </div>\n          </div>\n        </li>\n        <li class=\"align-top\">\n          <div class=\"item-content\">\n            <div class=\"item-media\"><i class=\"icon material-icons\">chat_bubble_outline</i></div>\n            <div class=\"item-inner\">\n              <div class=\"item-title label\">معلومات اضافية</div>\n              <div class=\"item-input\">\n                <textarea class=\"resizable\"></textarea>\n              </div>\n            </div>\n          </div>\n        </li>\n      </ul>\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/contact-page/contact-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPageComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ContactPageComponent = (function (_super) {
    __extends(ContactPageComponent, _super);
    function ContactPageComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ContactPageComponent;
}(__WEBPACK_IMPORTED_MODULE_0__framework7__["e" /* F7PageComponent */]));
ContactPageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* Component */])({
        selector: 'app-contact-page',
        template: __webpack_require__("../../../../../src/app/contact-page/contact-page.component.html"),
        styles: [__webpack_require__("../../../../../src/app/contact-page/contact-page.component.css")]
    })
], ContactPageComponent);

//# sourceMappingURL=contact-page.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "[data-page=\"home\"] .swiper-container {\n  font-size: 18px;\n  height: 100px;\n  z-index: 20;\n}\n\n[data-page=\"home\"] .swiper-container .swiper-slide {\n    border: none;\n    background-size:     cover;\n    background-repeat:   no-repeat;\n    background-position: center center;\n}\n\n/*.vjs-poster {\n    display: block!important;\n    background-image: url(../../assets/img/oceans.png)!important;\n}*/\n\n.pull-to-refresh-layer {\n    margin-top: -40px;\n}\n\n.panel .navbar {\n    height: 76px;\n}\n\n.panel .navbar-inner {\n    padding-top: 20px;\n}\n\n.panel .panel-status-overlay {\n    position: absolute;\n    height: 30px;\n    width: 100%;\n    z-index: 10;\n    background: rgba(54,92,163,1);\n    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(54,92,163,1)), color-stop(66%, rgba(54,92,163,1)), color-stop(100%, rgba(54,92,163,0)));\n    background: linear-gradient(to bottom, rgba(54,92,163,1) 0%, rgba(54,92,163,1) 66%, rgba(54,92,163,0) 100%);\n    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#365ca3', endColorstr='#365ca3', GradientType=0 );\n}\n\n[data-page=\"home\"] .swiper-container .swiper-pagination-bullets .swiper-pagination-bullet {\n    margin: 0px 3px;\n}\n\n[data-page=\"home\"] .swiper-pagination-bullet {\n    width: 6px;\n    height: 6px;\n    display: inline-block;\n    border-radius: 100%;\n    /* background: transparent; */\n    opacity: 0.2;\n    /* border: 1px solid white !important; */\n\n    background: #c7c7c7;\n    border: 1px solid #9d9d9d !important;\n}\n\n[data-page=\"home\"] .swiper-pagination-bullet-active {\n    opacity: 1;\n    /* background: white; */\n    background: none repeat scroll 0 0 #f18a1f;\n    border: 1px solid #d3730f;\n}\n\n[data-page=\"home\"] #swiper-slide-1 {\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/img/slide-1.jpg")) + ");\n}\n\n[data-page=\"home\"] #swiper-slide-2 {\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/img/slide-2.jpg")) + ");\n}\n\n[data-page=\"home\"] #swiper-slide-3 {\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/img/slide-3.jpg")) + ");\n}\n\n[data-page=\"home\"] .navbar-through.pages .page-content {\n    padding-top: 0px;\n}\n\n[data-page=\"home\"] .open-panel {\n    position: absolute;\n    color: white;\n    z-index: 30;\n    opacity: 0.8;\n    padding: 6px;\n    margin-top: 30px;\n    padding-right: 18px;\n    padding-bottom: 18px;\n}\n\n[data-page=\"home\"] .search {\n    position: absolute;\n    color: white;\n    z-index: 30;\n    opacity: 0.8;\n    padding: 6px;\n    margin-top: 30px;\n    padding-left: 18px;\n    padding-bottom: 18px;\n    left: 0px;\n}\n\n.video-js .vjs-poster {\n    background-image: url(" + escape(__webpack_require__("../../../../../src/assets/img/oceans.png")) + ");\n    background-repeat: no-repeat;\n    background-position: 50% 50%;\n    background-size: contain;\n    background-color: #000000;\n    /* display: block!important; */\n}\n\n.video-js *:focus {\n    outline-color: transparent;\n}\n\n.card-title {\n    font-family: \"SEC_GE_Bold\";\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    margin-bottom: 8px;\n    margin-top: 8px;\n    margin-left: 8px;\n    margin-right: 8px;\n    font-weight: bold;\n    pointer-events: none;\n}\n\n.card-content {\n    font-family: \"SEC_GE_Light\";\n}\n\n.card-footer {\n    font-family: \"SEC_GE_Medium\";\n    font-size: 14px;\n}\n\n.card-footer i {\n    font-size: 14px;\n    vertical-align: sub;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page\">\n  <div class=\"page-content\">\n\n    <a href=\"#\" class=\"open-panel link icon-only\" data-panel=\"right\"><i class=\"icon icon-bars\"></i></a>\n\n    <a href=\"#\" class=\"search link icon-only\" data-panel=\"\"><i class=\"material-icons\">search</i></a>\n\n    <div id=\"news-slider\" style=\"opacity: 0\" [style.height.px]=\"height\" class=\"swiper-container ks-carousel-slider\">\n      <div class=\"swiper-pagination swiper-pagination-c1\"></div>\n      <div class=\"swiper-wrapper\">\n        <div id=\"swiper-slide-1\" class=\"swiper-slide\"></div>\n        <div id=\"swiper-slide-2\" class=\"swiper-slide\"></div>\n        <div id=\"swiper-slide-3\" class=\"swiper-slide\"></div>\n        <!-- <div class=\"swiper-slide\">Slide 4</div>\n        <div class=\"swiper-slide\">Slide 5</div>\n        <div class=\"swiper-slide\">Slide 6</div>\n        <div class=\"swiper-slide\">Slide 7</div>\n        <div class=\"swiper-slide\">Slide 8</div>\n        <div class=\"swiper-slide\">Slide 9</div>\n        <div class=\"swiper-slide\">Slide 10</div> -->\n      </div>\n    </div>\n\n    <div class=\"pull-to-refresh-content\">\n      <div class=\"pull-to-refresh-layer\" style=\"opacity: 0\">\n        <div class=\"preloader\"></div>\n        <div class=\"pull-to-refresh-arrow\"></div>\n      </div>\n      <div class=\"card ks-card-header-pic\" id=\"first-card\" style=\"opacity: 0\">\n        <div style=\"padding: 0\" valign=\"bottom\" class=\"card-header color-white no-border\">\n          <video id=\"example_video_1\" class=\"video-js vjs-default-skin\" controls preload=\"metadata\" width=\"640\" height=\"264\" poster=\"http://vjs.zencdn.net/v/oceans.png\"\n            data-setup='{\"fluid\": true, \"nativeControlsForTouch\": false}'>\n            <source src=\"http://vjs.zencdn.net/v/oceans.mp4\" type=\"video/mp4\" />\n            <source src=\"http://vjs.zencdn.net/v/oceans.webm\" type=\"video/webm\" />\n            <source src=\"http://vjs.zencdn.net/v/oceans.ogv\" type=\"video/ogg\" />\n            <p class=\"vjs-no-js\">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\"\n                target=\"_blank\">supports HTML5 video</a></p>\n          </video>\n          <p class=\"card-title\">\n            \"السعودية لشراء الطاقة\" تحصل على رخصة \"المشتري الرئيس\"\n          </p>\n        </div>\n        <div class=\"card-content\">\n          <div class=\"card-content-inner\">\n            <p class=\"color-gray\">أمس الساعة ‏02:30 م‏</p>\n            <p>أعلنت الشركة السعودية لشراء الطاقة \"المشتري الرئيس\" إحدى الشركات التابعة للشركة السعودية للكهرباء أنها تسلمت\n              رخصة \"المشتري الرئيس\" من هيئة تنظيم الكهرباء والإنتاج المزدوج، مؤكدة أن حصولها على الرخصة يمكّنها من مزاولة\n              جميع أنشطة ومهام \"المشتري الرئيس\"...\n            </p>\n          </div>\n        </div>\n        <div class=\"card-footer\">\n          <a class=\"link\"><i class=\"material-icons\">favorite</i> أعجبني</a>\n          <a routerLink=\"news\" class=\"link\">قراءة المزيد <i class=\"material-icons\">more</i></a></div>\n      </div>\n\n      <div id=\"home-rest\" style=\"opacity: 0\">\n        <div class=\"card\">\n          <div class=\"card-header\">آخر الأخبار</div>\n          <div class=\"card-content\">\n            <div class=\"list-block media-list\">\n              <ul>\n                <li class=\"item-content\">\n                  <div class=\"item-media\"><img src=\"http://lorempixel.com/88/88/fashion/4\" width=\"44\" /></div>\n                  <div class=\"item-inner\">\n                    <div class=\"item-title-row\">\n                      <div class=\"item-title\">Yellow Submarine</div>\n                    </div>\n                    <div class=\"item-subtitle\">Beatles</div>\n                  </div>\n                </li>\n                <li class=\"item-content\">\n                  <div class=\"item-media\"><img src=\"http://lorempixel.com/88/88/fashion/5\" width=\"44\" /></div>\n                  <div class=\"item-inner\">\n                    <div class=\"item-title-row\">\n                      <div class=\"item-title\">Don't Stop Me Now</div>\n                    </div>\n                    <div class=\"item-subtitle\">Queen</div>\n                  </div>\n                </li>\n                <li class=\"item-content\">\n                  <div class=\"item-media\"><img src=\"http://lorempixel.com/88/88/fashion/6\" width=\"44\" /></div>\n                  <div class=\"item-inner\">\n                    <div class=\"item-title-row\">\n                      <div class=\"item-title\">Billie Jean</div>\n                    </div>\n                    <div class=\"item-subtitle\">Michael Jackson</div>\n                  </div>\n                </li>\n              </ul>\n            </div>\n          </div>\n          <div class=\"card-footer\"> <span>January 20, 2015</span><span>5 comments</span></div>\n        </div>\n\n        <div class=\"content-block-title\">Simple Cards</div>\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-content-inner\">This is simple card with plain text. But card could contain its own header, footer, list view, image, and any\n              elements inside.\n            </div>\n          </div>\n        </div>\n        <div class=\"card\">\n          <div class=\"card-header\">Card header</div>\n          <div class=\"card-content\">\n            <div class=\"card-content-inner\">Card with header and footer. Card header is used to display card title and footer for some additional information\n              or for custom actions.</div>\n          </div>\n          <div class=\"card-footer\">Card Footer</div>\n        </div>\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-content-inner\">Another card. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat sem est, non tincidunt\n              ligula volutpat sit amet. Mauris aliquet magna justo. </div>\n          </div>\n        </div>\n        <div class=\"content-block-title\">Styled Cards</div>\n        <div class=\"card ks-card-header-pic\">\n          <div style=\"background-image:url(http://lorempixel.com/1000/600/people/6/)\" valign=\"bottom\" class=\"card-header color-white no-border\">Lorem Ipsum</div>\n          <div class=\"card-content\">\n            <div class=\"card-content-inner\">\n              <p class=\"color-gray\">Posted on January 21, 2015</p>\n              <p>Quisque eget vestibulum nulla. Quisque quis dui quis ex ultricies efficitur vitae non felis. Phasellus quis\n                nibh hendrerit...\n              </p>\n            </div>\n          </div>\n          <div class=\"card-footer\"><a href=\"#\" class=\"link\">Like</a><a href=\"#\" class=\"link\">Read more</a></div>\n        </div>\n        <div class=\"content-block-title\">Facebook Cards</div>\n        <div class=\"card ks-facebook-card\">\n          <div class=\"card-header no-border\">\n            <div class=\"ks-facebook-avatar\"><img src=\"http://lorempixel.com/68/68/people/1/\" width=\"34\" height=\"34\" /></div>\n            <div class=\"ks-facebook-name\">John Doe</div>\n            <div class=\"ks-facebook-date\">Monday at 3:47 PM</div>\n          </div>\n          <div class=\"card-content\"> <img src=\"http://lorempixel.com/1000/700/nature/8/\" width=\"100%\" /></div>\n          <div class=\"card-footer no-border\"><a href=\"#\" class=\"link\">Like</a><a href=\"#\" class=\"link\">Comment</a><a href=\"#\" class=\"link\">Share</a></div>\n        </div>\n        <div class=\"card ks-facebook-card\">\n          <div class=\"card-header\">\n            <div class=\"ks-facebook-avatar\"><img src=\"http://lorempixel.com/68/68/people/1/\" width=\"34\" height=\"34\" /></div>\n            <div class=\"ks-facebook-name\">John Doe</div>\n            <div class=\"ks-facebook-date\">Monday at 2:15 PM</div>\n          </div>\n          <div class=\"card-content\">\n            <div class=\"card-content-inner\">\n              <p>What a nice photo i took yesterday!</p><img src=\"http://lorempixel.com/1000/700/nature/8/\" width=\"100%\" />\n              <p class=\"color-gray\">Likes: 112 &nbsp;&nbsp; Comments: 43</p>\n            </div>\n          </div>\n          <div class=\"card-footer\"><a href=\"#\" class=\"link\">Like</a><a href=\"#\" class=\"link\">Comment</a><a href=\"#\" class=\"link\">Share</a></div>\n        </div>\n        <div class=\"content-block-title\">Cards With List View</div>\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"list-block\">\n              <ul>\n                <li>\n                  <a href=\"#\" class=\"item-link item-content\">\n                    <div class=\"item-media\"><i class=\"icon icon-f7\"></i></div>\n                    <div class=\"item-inner\">\n                      <div class=\"item-title\">Link 1</div>\n                    </div>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"#\" class=\"item-link item-content\">\n                    <div class=\"item-media\"><i class=\"icon icon-f7\"></i></div>\n                    <div class=\"item-inner\">\n                      <div class=\"item-title\">Link 2</div>\n                    </div>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"#\" class=\"item-link item-content\">\n                    <div class=\"item-media\"><i class=\"icon icon-f7\"></i></div>\n                    <div class=\"item-inner\">\n                      <div class=\"item-title\">Link 3</div>\n                    </div>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"#\" class=\"item-link item-content\">\n                    <div class=\"item-media\"><i class=\"icon icon-f7\"></i></div>\n                    <div class=\"item-inner\">\n                      <div class=\"item-title\">Link 4</div>\n                    </div>\n                  </a>\n                </li>\n                <li>\n                  <a href=\"#\" class=\"item-link item-content\">\n                    <div class=\"item-media\"><i class=\"icon icon-f7\"></i></div>\n                    <div class=\"item-inner\">\n                      <div class=\"item-title\">Link 5</div>\n                    </div>\n                  </a>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { F7PageComponent } from '../../components/page/page.component';


var HomeComponent = (function (_super) {
    __extends(HomeComponent, _super);
    function HomeComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.newsSliderAspectRatio = 1 / 2;
        return _this;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        _super.prototype.ngOnInit.call(this);
        this.$newsSlider = this.$page.find("#news-slider");
        new Swiper(this.$newsSlider[0], {
            loop: true,
            spaceBetween: 0,
            observer: true,
            pagination: ".swiper-pagination-c1"
        });
        var cardTitle = $(this.$page.find(".card-title")[0]);
        this.myPlayer = videojs('example_video_1');
        var $controlBar = $("#example_video_1 .vjs-control-bar");
        // debugger;
        // $controlBar.attrchange(
        //   {
        //     callback: (event) => {
        //       Logger.log("Changed");
        //       if ($controlBar.hasClass("vjs-user-inactive")) {
        //         Logger.log("Inactive");
        //         cardTitle.animate({ "padding-bottom": "0px" }, 'fast'); //.fadeOut();
        //       }
        //       else {
        //         Logger.log("Active");
        //         cardTitle.animate({ "padding-bottom": "30px" }, 'fast'); //.fadeOut();
        //       }
        //     }
        //   });
        // $(".card-header").on("tap", (event) => {
        //   Logger.log("TAP");
        //   this.myPlayer.pause();
        // });
        this.myPlayer.on('pause', function () {
            // Logger.log("Paused");
            // cardTitle.fadeIn();
        });
        this.myPlayer.on('play', function () {
            // Logger.log("Play");
            cardTitle.animate({ "padding-bottom": "30px" }, 'fast').fadeOut();
            _this.myPlayer.play();
        });
        this.myPlayer.on('ended', function () {
            // Logger.log("Play");
            cardTitle.animate({ "padding-bottom": "30px" }, 'fast').fadeIn();
        });
        this.myPlayer.on('seeked', function () {
            // Logger.log("Seeked");
        });
        this.myPlayer.on('useractive', function () {
            if (_this.myPlayer.paused() == false)
                cardTitle.animate({ "padding-bottom": "30px" }, 'fast'); //.fadeOut();
        });
        this.myPlayer.on('userinactive', function () {
            if (_this.myPlayer.paused() == false)
                cardTitle.animate({ "padding-bottom": "0px" }, 'fast'); //.fadeOut();
        });
        setTimeout(function () {
            $("#news-slider").animate({ opacity: 1 }, 'slow', function () {
                setTimeout(function () {
                    $("#first-card").animate({ opacity: 1 }, 'slow', function () {
                        $("#home-rest").animate({ opacity: 1 }, 'slow');
                    });
                }, 500);
            });
        }, 500);
        this.$pullToRefresh = Dom7(".pull-to-refresh-content");
        this.$pullToRefresh.on('ptr:pullstart', function () {
            _this.$pullToRefresh.find(".pull-to-refresh-layer").animate({ opacity: 1 }, 'fast');
        });
        this.$pullToRefresh.on('ptr:refresh', function () {
            setTimeout(function () {
                __WEBPACK_IMPORTED_MODULE_0__app_component__["a" /* AppComponent */].F7App.pullToRefreshDone();
                _this.$pullToRefresh.find(".pull-to-refresh-layer").animate({ opacity: 0 }, 'fast');
            }, 4000);
        });
        Dom7(".panel-right").css("opacity", 1);
        this.onResize();
        // Logger.log("Child ngOnInit");
    };
    HomeComponent.prototype.onPageInit = function (page) {
    };
    HomeComponent.prototype.onPageScroll = function (scrollY) {
        if (scrollY > 0)
            __WEBPACK_IMPORTED_MODULE_1__framework7__["b" /* F7AppComponent */].f7App.destroyPullToRefresh(this.$pullToRefresh);
        else
            __WEBPACK_IMPORTED_MODULE_1__framework7__["b" /* F7AppComponent */].f7App.initPullToRefresh(this.$pullToRefresh);
    };
    HomeComponent.prototype.onResize = function (event) {
        if (event === void 0) { event = null; }
        var $$ = Dom7;
        this.height = this.$newsSlider.width() * this.newsSliderAspectRatio;
    };
    return HomeComponent;
}(__WEBPACK_IMPORTED_MODULE_1__framework7__["e" /* F7PageComponent */]));
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* HostListener */])('window:resize', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], HomeComponent.prototype, "onResize", null);
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_11" /* Component */])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/home.component.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_2__angular_core__["Y" /* ViewEncapsulation */].None
    })
], HomeComponent);

//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/news-page/news-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "[data-page=\"news-page\"] p {\n    font-family: \"SEC_GE_Light\";\n}\n\n[data-page=\"news-page\"] .title {\n    font-family: \"SEC_GE_Bold\";\n    font-weight: bold;\n}\n\n[data-page=\"news-page\"] .publish-data {\n    font-family: \"SEC_GE_Light\";\n    color: #9e9e9e;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-page/news-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page tabbar-labels-fixed\">\n  <div class=\"navbar\">\n    <div class=\"navbar-inner\">\n      <div class=\"left\"><a href=\"#\" class=\"link open-panel icon-only\" data-panel=\"right\"><i class=\"icon icon-bars\"></i></a></div>\n      <div class=\"center\">          \"السعودية لشراء الطاقة\" تحصل على رخصة \"المشتري الرئيس\" </div>\n      <div class=\"right\"><a routerLink=\"home\" class=\"back link icon-only\"><i class=\"material-icons\">arrow_back</i></a></div>\n    </div>\n  </div>\n  <div class=\"toolbar tabbar tabbar-labels\">\n    <div class=\"toolbar-inner\">\n      <a href=\"#tab-1\" class=\"show-tab-1 tab-link active\"><i class=\"icon material-icons\">chrome_reader_mode</i><span class=\"tabbar-label\">الخبر</span></a>\n      <a href=\"#tab-2\" class=\"show-tab-2 tab-link\"><i class=\"icon material-icons\">comment</i><span class=\"tabbar-label\">التعليقات</span></a>\n      <a href=\"#tab-3\" class=\"show-tab-3 tab-link\"><i class=\"icon material-icons\">share</i><span class=\"tabbar-label\">شارك</span></a>\n    </div>\n    </div>\n  <div class=\"page-content\">\n    <div class=\"tabs-swipeable-wrap\" data-observer=\"true\">\n      <div class=\"tabs\">\n        <div id=\"tab-1\" class=\"tab active\">\n          <div class=\"content-block\">\n            <p class=\"title\">\n              \"السعودية لشراء الطاقة\" تحصل على رخصة \"المشتري الرئيس\"\n            </p>\n            <p class=\"publish-data\">أمس الساعة ‏02:30 م‏</p>\n            <p>أعلنت الشركة السعودية لشراء الطاقة \"المشتري الرئيس\" إحدى الشركات التابعة للشركة السعودية للكهرباء أنها تسلمت رخصة \"المشتري الرئيس\" من هيئة تنظيم الكهرباء والإنتاج المزدوج، مؤكدة أن حصولها على الرخصة يمكّنها من مزاولة جميع أنشطة ومهام \"المشتري الرئيس\"، ومن أبرزها طرح مشاريع شراء وبيع وتحويل الطاقة الكهربائية، وخطط استيرادها وتصديرها إلى خارج المملكة، بالإضافة إلى سرعة العمل على بدء تطوير أسواق تجارة الطاقة الكهربائية وخدماتها، وبيع وتطوير خدمات المنظومة الكهربائية خلال السنوات القادمة بالإضافة إلى الحصول على الوقود الأساسي والوقود الاحتياطي لتوريده إلى المرخص لهم بالإنتاج، وإدارة حساب صندوق الموازنة الذي يهدف إلى تغطية العجز بين الإيراد المطلوب والإيراد الفعلي.\nوبيّن الأستاذ أسامة بن عبدالوهاب خوندنة الرئيس التنفيذي للشركة السعودية لشراء الطاقة \"المشتري الرئيس\"، بأن الشركة تخطط لتطوير خدمات سوق الكهرباء، بما يتناسب مع خارطة الطريق التي وضعها المنظم في مجال صناعة الطاقة الكهربائية بالمنطقة، لاسيما وأن المملكة العربية السعودية تنتج أكثر من ربع إنتاج الدول العربية مجتمعة من الكهرباء، مشيراً إلى دخول المشتري الرئيس في اتفاقيات لشراء الطاقة وتحويلها، والدخول في عمليات البيع المستمر، وبيع الطاقة الكهربائية بالجملة للمرخص لهم للبيع بالتجزئة، وإلى كبار المستهلكين.\nيُذكر بأن الشركة السعودية لشراء الطاقة \"المشتري الرئيس\" ذات مسؤولية محدودة مملوكة بالكامل للشركة السعودية للكهرباء وأنها حصلت على الموافقات اللازمة من الجهات النظامية المختصة تنفيذاً للأمر السامي الكريم والقاضي باستقلالية المشتري الرئيس، ضمن رؤية المملكة العربية السعودية 2030، وخططها لإعادة هيكلة الاقتصاد الوطني، والتي من ضمنها تحقيق التنافسية في قطاع الطاقة الكهربائية بالمملكة.</p>\n          </div>\n        </div>\n        <div id=\"tab-2\" class=\"tab\">\n          <div class=\"content-block\">\n            <p>\n\n\nتمكن 25 مهندساً وفنياً سعودياً بمحطة توليد القرية المركبة التابعة للشركة السعودية للكهرباء في تحقيق إنجاز يُعد هو الأول على مستوى المملكة في غضون 11 يوم عمل فقط، وذلك من خلال إجراء عمليات صيانة فنية بالمحطة، وفقاً للقواعد والأنظمة الهندسية والتقنية المعتمدة من الشركة المصنعة مع الالتزام بكافة معايير الصحة والأمن والسلامة المهنية العالمية. وأوضحت الشركة أن كوادر المحطة من مهندسين وفنيين نجحوا في استبدال ضاغط الهواء بالتوربينات الغازية بعد إجراء عدد من الدراسات المستفيضة مع الشركة المصنعة لمناقشة الحلول الممكنة لتجنب المشاكل أثناء عملية التشغيل الاعتيادية بالمحطة، والتي قد تحدث تلف لريش ضاغط الهواء بسبب عمليات الارتداد أو وجود مشاكل في الريش، حيث عمل الفريق على وضع خطة للعمل لإجراء الصيانة المطلوبة، دون الاستعانة بأي عنصر من عناصر الشركة المصنعة. وأكدت \"السعودية للكهرباء\" أن من أهم فوائد هذا العمل هو اكتساب كوادرها الوطنية الموجودة بالمحطات القدرة والمعرفة على استبدال أياً من ريش مراحل ضاغط الهواء لهذا الطراز من التوربينات حيث أصبحت مستعدة لنقل الخبرة المكتسبة لمحطات الشركة وخارجها، مبينة أن اكتساب هذه القدرات المميزة هو نتاج مراحل تدريب وتأهيل المهندسين والفنيين للوصول إلى أعلى المستويات التي تؤهلهم للقيام بمثل هذه الأعمال الصعبة وفق مبادرات التحول الاستراتيجي بالشركة والتي تتوافق مع رؤية المملكة 2030، والتي تهدف إلى دعم تنوع الاقتصاد الوطني، والاعتماد على شباب الوطن في كافة مواقع العمل والبناء، بالإضافة الى تحقيق وفر مالي يقدر بـ 11 مليون ريال. يُذكر أن محطة القرية المركبة تُعد واحدة من أكثر محطات التوليد كفاءة في العالم، والتي يتم إدارتها وتشغيلها بأيدي خبراء ومهندسين وفنيين سعوديين بنسبة مئوية تصل إلى 84 في المائة من إجمالي فريق عمل المحطة، فيما سيتم خلال الثلاث سنوات القادمة إعادة هيكلة فريق العمل ليصبح من الكوادر والكفاءات السعودية بالكامل، بعد أن نجحوا في الوصول بالمحطة إلى تحقيق أرقام قياسية سواء فيما يتعلق بالكفاءة التشغيلية أو بتوفير الوقود المكافئ واستخدام أحدث التقنيات في مجال توليد الطاقة الكهربائية بنظام الدورة المركبة.\n\n            </p>\n          </div>\n        </div>\n        <div id=\"tab-3\" class=\"tab\">\n          <div class=\"content-block\">\n            <p>\n              \n\nتنطلق، غداً الخميس 13/7/2017م، فعاليات ملتقى البيئة 2017م، الذي تنظمه الشركة السعودية للكهرباء، بمركز الامير سلطان بن عبد العزيز للعلوم والتقنية \"سايتك\" بالخبر، بهدف إبراز الجهود المبذولة للمحافظة على البيئة، وتعزيز التواصل وخلق فرص لتبادل المعارف والخبرات بين الجهات ذات العلاقة، وذلك بحضور عدد من المسؤولين وممثلي الجهات الحكومية والمختصين في هذا المجال.\nوأوضحت الشركة السعودية للكهرباء أن الملتقى يشهد مشاركة عدد كبير من الجهات الحكومية، من أبرزها أمانة المنطقة الشرقية، وشركة ارامكو السعودية، والهيئة العامة للأرصاد وحماية البيئة، وجامعة الإمام عبد الرحمن الفيصل بالدمام، ووحدة حماية البيئة بهيئة حقوق الإنسان، وغيرها من الجهات ذات العلاقة في المجتمع، مؤكده أن الملتقى يُعد فرصة هامة لتبادل الخبرات في مجال المحافظة على البيئة ومنصة بارزة للاطلاع على تجارب الأخرين والاستفادة من الطرق والوسائل الحديثة في الحفاظ على البيئة ورفع مستوى الاهتمام بها، وتعزيز وتطوير الشراكات مع المختصين في إطار سعيها للاهتمام والمحافظة على البيئة، ودورها الداعم للمنظمات والجهات والهيئات والجمعيات الوطنية والمناسبات ذات العلاقة.\nوبينت الشركة أن الملتقى سوف يستعرض عدد من أوراق العمل الخاصة ببعض الجهات المشاركة، والتي تتمحور حول أثر التلوث البيئي في الأوساط الحيوية، وطرق إعادة تدوير مخلفات المنازل وتحويلها الى أسمدة باستخدام تقنيات حديثة، وأثر التسريبات البترولية على البيئة في محطات التوليد، وغيرها من المشاركات العلمية الثرية، والتي سوف يقدمها نخبة من المستشارين والمختصين في مجال البيئة.\nيُذكر أن \"السعودية للكهرباء\" تعمل على تعزيز الاعتماد على مصادر الطاقة النظيفة والاستخدام الامثل للموارد واعادة الاستخدام والتدوير، واتخاذ تدابير التقليل من النفايات بالإضافة إلى الامتثال لكافة القوانين واللوائح البيئية السائدة ، حيث يتم مراقبة مستوى التوافق مع المقاييس البيئية العالمية والمقاييس البيئية المحدثة الصادرة عام 2014م من الهيئة العامة للأرصاد وحماية البيئة من خلال التدقيق الدوري المتوافق مع ISO 19001: 2011 ، و قامت الشركة ببدء انتاج الطاقة من مصادر الطاقة الشمسية وطاقة الرياح وتطبيق برامج ترشيد استهلاك الطاقة الكهربائية بجميع المواقع الإدارية بالشركة وتوفير استهلاك الطاقة في مكاتبها من خلال تطبيق برامج وحلول تقنية للتحكم في تشغيل الخدمة في مبانيها، وكذلك تطبيق برامج ترشيد استهلاك الورق ، بالإضافة إلى تطبيق العزل الحراري في المباني، والذي ساهم في تقليل الفاقد في استهلاك الطاقة الكهربائية.\n</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/news-page/news-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPageComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NewsPageComponent = (function (_super) {
    __extends(NewsPageComponent, _super);
    function NewsPageComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NewsPageComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        var $$ = Dom7;
        // $$('.show-tab-1').on('click', function () {
        //     AppComponent.F7App.showTab('#tab-1');
        // });
        // $$('.show-tab-2').on('click', function () {
        //     AppComponent.F7App.showTab('#tab-2');
        // });
        // $$('.show-tab-3').on('click', function () {
        //     AppComponent.F7App.showTab('#tab-3');
        // });    
    };
    NewsPageComponent.prototype.onPageInit = function () {
        __WEBPACK_IMPORTED_MODULE_0__app_component__["a" /* AppComponent */].F7App.showTab('#tab-1');
    };
    return NewsPageComponent;
}(__WEBPACK_IMPORTED_MODULE_1__framework7__["e" /* F7PageComponent */]));
NewsPageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_11" /* Component */])({
        selector: 'app-news-page',
        template: __webpack_require__("../../../../../src/app/news-page/news-page.component.html"),
        styles: [__webpack_require__("../../../../../src/app/news-page/news-page.component.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_2__angular_core__["Y" /* ViewEncapsulation */].None
    })
], NewsPageComponent);

//# sourceMappingURL=news-page.component.js.map

/***/ }),

/***/ "../../../../../src/app/timeline/timelines-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div data-page=\"index\" class=\"page\">\n  <div class=\"page-content\">    \n    <div class=\"content-block-title\">timelines-page works!</div>\n    <a routerLink=\"/home\" class=\"external\">home</a>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/timeline/timelines-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__framework7__ = __webpack_require__("../../../../../src/@framework7/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelinesPageComponent; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var TimelinesPageComponent = (function (_super) {
    __extends(TimelinesPageComponent, _super);
    function TimelinesPageComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return TimelinesPageComponent;
}(__WEBPACK_IMPORTED_MODULE_0__framework7__["e" /* F7PageComponent */]));
TimelinesPageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* Component */])({
        selector: 'app-timelines-page',
        template: __webpack_require__("../../../../../src/app/timeline/timelines-page.component.html"),
    })
], TimelinesPageComponent);

//# sourceMappingURL=timelines-page.component.js.map

/***/ }),

/***/ "../../../../../src/assets/img/oceans.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "oceans.50a3991792355252d5a8.png";

/***/ }),

/***/ "../../../../../src/assets/img/slide-1.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "slide-1.9f8ec62c0cc2d91610d6.jpg";

/***/ }),

/***/ "../../../../../src/assets/img/slide-2.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "slide-2.5dfce218091025903f30.jpg";

/***/ }),

/***/ "../../../../../src/assets/img/slide-3.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "slide-3.32f7a247b11ac6da77f7.jpg";

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");



// if (environment.production) {
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
// }
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map